import { TestBed, async } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA}   from '@angular/core';
import { AppComponent } from './app.component';
import { Observable} from 'rxjs';
import {DataService} from './data.service';
describe('AppComponent', () => {
  let mockDataClass;

  beforeEach(async(() => {
    mockDataClass = {
      fetch(): Observable<any> {
        return new Observable();
      }
    }

    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [{provide: DataService, useValue: mockDataClass}],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
