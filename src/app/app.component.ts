import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { observable, timer, Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  data: any[] = [];
  currentTime = Date.now();
  timeToRefresh: number = 60;
  countdown;
  timer;
  polling;

  constructor(private service: DataService){}

  ngOnInit() {
    this.getSchedules();
    timer(60000, 60000)
    .subscribe( () => {
      this.getSchedules();
    });
   this.timer = timer(1000, 1000);
   this.countdown = this.timer.subscribe( () => {
     this.timeToRefresh = this.timeToRefresh - 1;
   })

  }

  getSchedules(): void {
    this.service.fetch()
    .subscribe( 
      (response) => {
        this.timeToRefresh = 60;
        this.data = response
      },
      () => {
        this.countdown.unsubscribe();
      }
    );
  }
}
